Organizational Security Policies Template
=================


Baseline organizational practices
-------------------

1. [Access policies](#7we3ntqs92fn)
    - [a. Access to where you work](#llduhfrf8ye0)
        - [i. The office and your desk](#5cy5di7ujjex)
        - [ii. A home office or personal workspace](#corws3uqqxe7)
    - [b. Access to physical documents](#nu5wl7mbnh5)
        - [i. Printed documents](#u9fp01cjk71z)
        - [ii. Destruction of papers](#ohynrrsowkja)
    - [c. Devices](#cneaewojyb0j)
        - [i. Number of devices](#4dhi7dsh04tw)
        - [ii. Access to devices](#wu4v2aytkvmo)
    - [d. Access to internet](#jnwl7ij2k1i5)
    - [e. Access to accounts](#v5nwlrejiitd)
        - [i. Two-Factor Authentication](#wtfwcz9ih5gb)
        - [ii. Password management](#6r1cvdme4q45)
        - [iii. G Suite practices](#ups1ji8w8qx)
        - [iv. Dropbox practices](#by1fkmcflt74)
        - [v. Slack practices](#elfwvyl6semr)
        - [vi. Trello practices](#fr8snfjnumo)
    - [b. Revoking access to organizational assets upon departure](#jsgpqgjiss5a)

2. [Data management policies](#qi8barmv5j12)
    - [a. Backing up data](#d1n4qtbvkrzi)
    - [b. Transferring data](#tcdwcnkg8nw6)
    - [c. Traveling](#kebchkxtl2yd)
    - [d. Deleting data](#3s9d98tikn3q)
    - [e. Device encryption for all devices](#g47kclkb06ga)
    - [f. Encryption for external media](#x4056g72bwvp)

3. [Communications policies & practices](#2mjrr3say9pg)
    - [a. Online communication](#v1i5hop72acs)
    - [b. Communicating with at-risk partners](#l3uv2efruza2)
    - [c. Web etiquette](#fld08f9e02f3)
    - [d. Web browsing](#lk3pokycowon)
    - [e. Digital security concerns](#gqy8gv5w1rzc)

4. [Travel policies and procedures](#cwhasyrukd3s)
    - [a. Devices](#66ctgfndt3o)
    - [b. Internet use](#wf4menzgiaqi)

5. [Creating and maintaining an engaged organizational culture](#6bqo8dqlb1im)

* * *

<a name="7we3ntqs92fn"></a>

### 1. Access policies

<a name="llduhfrf8ye0"></a>

#### a. Access to where you work

<a name="5cy5di7ujjex"></a>

##### I. The office and your desk

-   **Who has the keys, fobs, or PIN to enter the office?**

    -   Know how many copies of keys or fobs you have, and only provide
        them, or disclose PINs, to people who are trusted to remain in
        the office alone

    -   collect the keys or fobs upon staff departure, or change the PIN

    -   Avoid lending the keys or fobs to anyone who is not a permanent
        staff member, and avoid disclosing the PIN

-   **Locking up: Desks, doors, cabinets**

    -   Last one to leave the office locks up

    -   Staff should place papers and valuable hardware in a locked desk
        or cabinet if available

-   **Be aware of tailgating and impersonation: **“Social engineers” are
    people who attempt to manipulate others into taking actions, often
    using someone’s helpfulness or adherence to routine and respect for
    authority against them.

    A social engineer may attempt to use “tailgating” to enter a building.
Tailgating means that as you enter the building the social engineer
follows right behind you. Perhaps they rely on your politely holding the
door, or the social engineer will rummage through the bag they are
holding pretending to look for their key and they are swiped in when
someone takes pity on them.

    Impersonation is another way in which someone may enter the building.
Perhaps they dress as a delivery person or as someone waiting on a job
interview. In large enough organizations, it even works to imply they
are new to their job, or work in a different department than yours.

    Never feel awkward about asking why someone is where they are, or
questioning their work. If they are supposed to be there, they’ll
understand.

<a name="corws3uqqxe7"></a>

##### II. A home office or personal workspace

-   **Who has access to your home office?**

    Be aware of who is able to enter your home office, from family to
friends, ensure that they understand the confidentiality of your work.
If a stranger is in your home, like a housecleaner or plumber, please
try to avoid allowing access to your workspace. If the stranger needs
workspace access, take precautions before the visit (lock confidential
papers away, remove laptops and turn off computers).

-   **Locking up desks, drawers, cabinets**

    At the end of the work day, confidential documents should be locked up
and placed out of view if possible. Laptops and computers should be
turned off and placed out of sight.

<a name="nu5wl7mbnh5"></a>

#### b. Access to physical documents

<a name="u9fp01cjk71z"></a>

##### I. Printed documents

Any internal printed documents with {ORG} information should be
treated as confidential, and cared for appropriately. Do not leave
prints visible in public spaces, and please try to be aware of how
private particular information is before you print it -- especially if
you are creating copies.

Try not to bring printed versions of confidential documents home, and
avoid printed material when working in cafes and other public spaces.
Please never leave printed material unattended.

When in the office, please avoid leaving printed documents visibly on
top of your desk, and try to secure any documents in a locked desk or
cabinet before going home.<span id="anchor-6"></span>When in the
office, please avoid leaving printed documents visibly on top of your
desk, and try to secure any documents in a locked desk or cabinet
before going home.

<a name="ohynrrsowkja"></a>

##### II. Destruction of papers

If a document is printed, as soon as it is no longer needed please
destroy in a paper shredder as soon as possible.

<a name="cneaewojyb0j"></a>

#### c. Devices

<a name="4dhi7dsh04tw"></a>

##### I. Number of devices

Try to reduce the number of devices that have work information on
them, including the credentials to your email. Every new device with
confidential work accounts or data is a new device that will require
protection from loss, theft, and hacking. If possible, a smartphone
and computer (laptop, desktop) should be the only two devices used for
work information.

{ORG} has provisioned work laptops for staff to ensure that
confidential work information stays on those devices, and is not
transferred to private devices.

<a name="wu4v2aytkvmo"></a>

##### II. Access to devices

Ensure that you are the only one who can access the device containing
work information. If there is confidential data on the same laptop
that others who are not involved with {ORG} use, please remove it.

Include a PIN or passphrase to access any device you have with {ORG}
information on it, including smartphones.

When device encryption is enabled, a screen lock passphrase (or PIN)
is also created by default. Please see the [*device encryption
section*](#g47kclkb06ga) for further details.

<a name="jnwl7ij2k1i5"></a>

#### d. Access to internet

Places (trusted vs untrusted, only they have access = trusted)

-   Home
-   Office
-   Coworking

Security:

-   WPA2
-   Ethernet
-   Guest network

<a name="v5nwlrejiitd"></a>

#### e. Access to accounts

<a name="wtfwcz9ih5gb"></a>

##### I. Two-Factor Authentication

All programs must have two-step verification when it’s an option. This
includes for example:

- List of [services with two-factor authentication](https://twofactorauth.org/)

- Google Apps (Gmail, Drive, etc)
    - [Link to Google’s two-factor authentication how-to](https://www.google.com/landing/2step/help.html)
    - [Link to Google 2FA FAQ](https://docs.google.com/a/accessnow.org/document/d/1whY3W_JLXRytmjYxZdYVKcE9trei4eMXRGcchPoQG3Y/edit?usp=sharing)

- Dropbox
    -   [Link to Dropbox’s 2FA how-to](https://www.dropbox.com/en/help/363)

- Slack
    - [Link to Slack's 2FA how-to](https://get.slack.help/hc/en-us/articles/204509068-Set-up-two-factor-authentication)

- Trello
    - [Link to Trello’s 2FA how-to](http://help.trello.com/article/993-enabling-two-factor-authentication-for-your-trello-account)

Some services provide backup codes in case the device is lost and
stolen. Make sure that you save these in a safe place, in case of
emergency.

<a name="6r1cvdme4q45"></a>

##### II. Password management

- **Team Passwords**

    In order to keep passwords secure, {ORG} may consider storing organizational passwords in a team password manager that does not require sharing credentials insecurely (emailing, post-it notes, etc.). 

    {ORG’s preferred password manager should be noted here, including process for obtaining access}

    Each app should have a unique password and should follow the responsible password generation guidelines noted below.

-   **Personal Passwords**

    Responsible password creation and use can be better understood using the
following links:

    - [Eff's guide on creating strong passwords](https://ssd.eff.org/en/module/creating-strong-passwords)

    - [Security in a Box guide on passwords](https://securityinabox.org/en/guide/passwords)

    The primary takeaways are

    -   *Long*: over 15 characters
    -   *Strong*: upper and lower case letters, numbers, and symbols should be included
    -   *Always unique*: never reuse a password

    Intimidated by these requirements? Consider using this comic for
inspiration [*https://xkcd.com/936/*](https://xkcd.com/936/)

-   **Password Managers**

    In order to manage personal passwords and ensure that all your passwords
are long, strong, and unique, you should use a password manager.

    The recommended personal password manager is KeePassXC:

    - [EFF's guide on KeePassXC](https://ssd.eff.org/en/module/how-use-keepassxc)
    - [Security in a Box guide for KeePassX on Mac](https://securityinabox.org/en/guide/keepassx/os-x)
    - [Security in a Box guide for KeePassX on Windows](https://securityinabox.org/en/guide/keepassx/windows)

-   **Sharing Passwords**

    If you must share passwords with a colleague, please use a password
sharing strategy that reduces the risk that the password will be
recorded by a third-party or compromised.

    In particular, sharing it on a piece of paper works if the password is then inputted into the receiver’s personal KeePassX and the paper
shredded.

    Using third-party services like **Keyvault **is not recommended because you are sharing your password with a third-party and their network. The content is not end-to-end encrypted (end-to-end encryption ensures that only the sender and recipient know the content of the message).

    Using an end-to-end messaging application such as **Signal **or
**WhatsApp **could work. Please see the [*Mobile Messaging
section*](#gj182uwvt4z5) for further details.

<a name="ups1ji8w8qx"></a>

##### III. G Suite practices

Try to become familiar with Google Doc’s [*Sharing Settings*](https://support.google.com/drive/answer/2494893?co=GENIE.Platform%3DDesktop&hl=en).

If you're sharing a Google doc with the whole {ORG} team or want to
give everyone access, share the document with "Anyone with Link within
{ORG}" and then share explicitly with anyone else who needs it. This
ensures that you know exactly who has access to your Google Doc and
the information it contains.

Avoid sharing a Google doc with "Anyone With Link" because anyone
outside of the organization who finds the link will be able to access
the document, posing a security risk given the sensitive information
that is often inside of our Google Docs.

Also note that you are able to limit the amount of time in which
someone can access a document. If you want to allow someone access for
only a day, a month, or a year you can. Take advantage of these, and
other settings to protect your documents successfully.

<a name="by1fkmcflt74"></a>

##### IV. Dropbox Practices

The {ORG} will set up a Dropbox Business account to manage
organizational Dropbox use. Once set-up this must be the basis of
accessing Dropbox.

-   Only add Dropbox to devices you need access to work documents on --
    each new device used with Dropbox is a new device you will need
    to protect.
-   Become familiar with the [*security
    settings*](https://www.dropbox.com/account#security) and make
    adjustments accordingly, for instance:

    -   Keep an eye on what extension apps you have allowed to view your
        Dropbox data. Remove apps that are no longer in use.

    -   Receive notifications for when a new device has accessed the
        Dropbox account or a new app is added under the [*profile
        page*](https://www.dropbox.com/account#profile)

-   Be aware that Dropbox protects your data in transit to Dropbox
    servers and to those you share it with, but it does not protect the
    data itself if someone were to enter your account or your
    recipients’, or otherwise access the documents. If you need to
    protect the confidentiality of highly sensitive documents, please
    contact the Access Now Helpline
    ([*help@accessnow.org*](mailto:help@accessnow.org)) to
    discuss strategies.

<a name="elfwvyl6semr"></a>

##### V. Slack Practices

With a team-organization platform like Slack, attention to access
control is very important. Only staff that need access to
administrator controls should have it, and when team members leave,
their prompt removal is imperative. You can read more about the roles
of access for Slack [*here*](https://get.slack.help/hc/en-us/articles/201314026-Roles-and-permissions-in-Slack).

[*Two-factor Authentication*](https://get.slack.help/hc/en-us/articles/204509068-Set-up-two-factor-authentication) should be in place for all Slack users, combined with a unique, long and strong password.

Be aware that the default retention setting for Slack teams is to
retain all messages in channels and direct messages, for all team
members, for as long as the team exists. For paid teams,
Administrators can manage when messages are deleted (for example,
after a day, a week, or a month).

Please view [*Slack’s Privacy FAQs*](https://get.slack.help/hc/en-us/articles/203950296-Privacy-FAQs) for more information.

<a name="fr8snfjnumo"></a>

##### VI. Trello Practices

Trello allows users to set the [*visibility of a board*](http://help.trello.com/article/789-changing-the-visibility-of-a-board-to-public-private-or-team) to public, private or team allowing only those you expect to view your work to view it. You can also [*implement two-step authentication*](http://help.trello.com/article/993-enabling-two-factor-authentication-for-your-trello-account) for the accounts.

[*Trello Business Class*](http://help.trello.com/article/714-business-class-user-guide) gives you a few more options, and may be a better way to control your privacy. Business Class allows access to admin privilege settings, permissions, and easy deactivation of team members. However, expect your projects to be archived for the long term.

<a name="jsgpqgjiss5a"></a>

#### e. Revoking access to organizational assets upon departure

-   On the last day of employment, the {Operations Director} will ask
    the employee to put on an out of office noting they are no longer
    working with {ORG} and providing information on whom to contact.
    We also ask that they forward their email to the
    appropriate colleague. {Include process in which ORG removes
    access to email and locks employee out of other accounts}

-   The email will remain active for 2-3 months, at which time it will
    be shut down

-   {Note ORG accounts to be shut down, ownership changed}

-   All organizational property should be returned, and any rented
    offices keys/cards returned and vacated.

-   Please see the {Employee Exit Process} for a detailed overview of
    the exit process.

<a name="qi8barmv5j12"></a>

### 2. Data management policies

<a name="d1n4qtbvkrzi"></a>

#### a. Backing up data

Backups of personal documents and systems can be completed on external
media, like a hard drive or USB. Ensure you have [*encrypted the
drive*](#x4056g72bwvp) before placing any backups on it. Please back up
routinely, at least once a month.

Backup strategies for:

- [Mac OSX: Time Machine](https://support.apple.com/en-us/HT201250)

- [iOS](https://support.apple.com/en-us/HT203977)

<a name="tcdwcnkg8nw6"></a>

#### b. Transferring data

Transfer confidential information securely either by placing on an
external media device and directly handing off to the intended
recipient, or by encrypting an email and sending the information.

Try to ensure all parties involved know to expect the incoming data
before they access it. Knowing to expect an attachment helps to prevent
phishing attempts where an attachment is the source of malware.

-   **Email**

    Transferring data over the internet is possible through encrypted
    email, or by encrypting a file and sending it. Ensure the person who
    is receiving the attachment knows the files are coming. Don’t forget
    that Subject lines of emails, as well as the To and From, are not
    encrypted, so never put anything private there.

-   **External Media (USB, hard drive, etc.)**

    Before plugging in, know who had the hard drive last and approximately what is on it. (Remember, never plug any external media into your computer that you don’t know its origins)

Any external media can be used to transfer data, but after the
transfer, the data should be deleted from the external media. Encrypt
confidential files, or the entire drive, and while in transit, keep
the external media on your person as much as possible.

<a name="kebchkxtl2yd"></a>

#### c. Traveling

When traveling, it is essential that one takes extra precautions. This
section is only about data management. For full travel recommendations,
see [*Travel Policies and Procedures*](#diwraoi9bck) below.

-   If you must use public or unsecured wifi, always use a VPN (Virtual
    Private Network). This is true for all devices,
    including smartphones.

-   Recognize that devices may be searched, so ensure full disk
    encryption is enabled on everything (including on smartphones),
    and turn devices off when moving through borders. Note that full
    disk encryption only protects you while your machine is
    entirely off. *Please see below for instructions on [*how to
    encrypt*](#g47kclkb06ga).

-   To transport data securely, ensure all external media is encrypted.
    Double check passcodes are enabled on your laptops and phones.

-   If you are transferring data through email, encrypt the message and
    the the file.

-   Try not to bring devices that have private or compromising data on
    your travels. If you must, leave any information you will not be
    using at home.

<a name="3s9d98tikn3q"></a>

#### d. Deleting data

When information is deleted, it is still vulnerable to recovery if not
specifically overwritten. If it is important the files are not
recoverable, please be sure to use secure deletion practices.

- [Mac OSX](https://ssd.eff.org/en/module/how-delete-your-data-securely-mac-os-x)

- [Windows](https://ssd.eff.org/en/module/how-delete-your-data-securely-windows)

<a name="g47kclkb06ga"></a>

#### e. Device encryption for all devices

Device encryption (also called full-disk encryption or FDE) will protect
your computer, mobile phone, and tablets against physical access by
encrypting your hard drive if your device is lost or stolen. Your device
*must be turned off* for this to work.

Enable full disk encryption on all devices containing {ORG} information
and access (like email)

Encrypt your phone and tablets:

-   [*Android*](http://www.howtogeek.com/141953/how-to-encrypt-your-android-phone-and-why-you-might-want-to/)
-   [*iOS*](http://www.zdnet.com/article/how-to-turn-on-iphone-ipad-encryption-in-one-minute/)
-   [*Windows*](http://www.windowscentral.com/how-enable-device-encryption-windows-10-mobile)

Laptops / computers

-   Mac: encrypt with [*FileVault*](https://support.apple.com/en-us/HT204837)
-   Windows: encrypt with [*BitLocker*](http://www.howtogeek.com/234826/how-to-enable-full-disk-encryption-on-windows-10/)
-   Linux: encrypt during installation, just opt in!

<a name="x4056g72bwvp"></a>

#### f. Encryption for external media

If USBs, hard drives, or other external media contain sensitive or
confidential information, ensure they are encrypted and kept in safe
locations.

-   Mac: encrypt with [*FileVault*](http://apple.stackexchange.com/a/181105)

-   Cross-platform: [*VeraCrypt*](https://securityinabox.org/en/guide/veracrypt/os-x)

After the information is no longer needed, make sure to [*delete it
securely*](#3s9d98tikn3q).

Never loan external media to friends or people outside of the
organization, and never plug a stranger’s USB or hard drive into your
machine.

<a name="2mjrr3say9pg"></a>

### 3. Communications policies & practices

When communicating with coworkers, members and partners, be aware of
their context. Is your coworker traveling? What country is the partner
or member in? Will it endanger the partner or member to receive messages
from your email? Be sure to take note when evaluating how best to
communicate.

<a name="v1i5hop72acs"></a>

#### a. Online communication

-   **Email**

    The To and From addresses in emails, as well as the Subject of the
    email, may be visible to third-party parties beyond the email
    providers and your intended recipients, so avoid putting sensitive
    information in those fields.

    Be mindful that your work email address may alert malicious actors to our partners’ existence and whereabouts. Consider other channels of contact if a more subtle approach is important.

-   **Video and voice chat**

    Voice communication can be a fast and efficient form of communication with the organization and with external partners. Options such as Google Hangouts (which allows [*maximum 25 participants*](https://support.google.com/plus/answer/1216376?hl=en)) and [*Jitsi Meet*](https://meet.jit.si/) are often satisfactory choices.

    Try to avoid using Skype as the default for video and chat
    communications. Skype has had a number of poor practices that make it easier for users to be targeted or compromised through it, including:

    - easy impersonation of contacts
    - being a separate program rather than a platform that runs in browser
        - When you open attachments and files on the desktop rather than in an online service (like Google Drive for example) you are putting yourself at greater risk of compromise
    - enabling contacts to find your geographic location (IP address)

-   **Mobile messaging**

    For mobile messaging, consider using **Signal**, an encrypted and
    easy-to-use mobile messaging application. Signal also has a desktop
    instant messaging option, making it as convenient to use as Hangouts.

    Try [*Signal support page*](https://support.signal.org/hc/en-us) for questions.

<a name="l3uv2efruza2"></a>

#### b. Communicating with at-risk partners

It is important to be aware of our partners’ individual security
contexts when we communicate with them. In trying to protect them and
their work, we should always (1) *follow their lead* and (2) *provide
communications options*.

No one will know their context and the threats they face better than
themselves. At the same time, they may have security needs that can
only be met by us having more secure communications practices. Letting
them know different ways that you can communicate with them -- using
non-{ORG} branded accounts, encrypting the content of communications,
protecting the location or identity of the partner, etc. - can help
them make a more informed decision.

We will be drafting some secure communications workflows for particular partners who we work with.

Please reach out to Access Now’s Helpline team ([*help@accessnow.org*](mailto:help@accessnow.org)) to craft a tailored strategy for any situation you think may require heightened practices, or if your partner may need support in implementing a particular practice.

<a name="fld08f9e02f3"></a>

#### c. Web etiquette

- **Public posts**

    When posting publicly, whether personal photos on social media
    accounts or promotional material for the organization, please be aware of the personal information that may be revealed. Do you have the
    permission of the people in the photo, or of the photographer, to use their photo? Check the backgrounds of pictures for revealing
information (password on a post-it? list of partners' emails?), be
sure never to reveal personally identifiable information when
discussing partners, and if you’re unsure if something qualifies as
private, choose not to post it.

- **Links and attachments**

    When sending links and attachments, try to alert the receiver to their being sent through a second channel, like a phone call or text
message. This lets the receiver be reasonably sure it is not a
phishing attack.

When receiving links and attachments, try to remain cautious and if
you are unsure of the message, or of the sender, do not click on the
link. Do your best to verify unsolicited links and attachments by
asking the sender through a separate channel if they did indeed send
the message.

<a name="lk3pokycowon"></a>

#### d. Web browsing

- **VPN use**

    When browsing the web on any untrusted or open wifi network, including while traveling, in cafes, airports, or hotels, please use a VPN (virtual private network) at all times.

    {ORG} will ensure a VPN account is created for you, and that you will receive support through the initial setup. [*VPNs provide a secure
connection to the internet*](http://lifehacker.com/5940565/why-you-should-start-using-a-vpn-and-how-to-choose-the-best-one-for-your-needs)
for you to browse safely. Your network traffic to websites and online
services will look like it is coming from a different location than
where you are, and all data that is sent and received will be protected
from snooping on the local network you are connected to (in the cafe,
airport, etc.).

    If desired, you can use a VPN at all times, even on your home or office network. VPNs may be used on all of your devices, including phones, laptops, tablets, and more.

-   **Browser extensions**

    Consider adding privacy enhancing browser extensions to block ads and trackers which can inadvertently deliver malware, and prefer website
connections to HTTPS. Examples include [*Privacy Badger*](https://www.eff.org/privacybadger), [*HTTPS Everywhere*](https://www.eff.org/https-everywhere), and adblockers such as uBlock Origin ([*Firefox*](https://addons.mozilla.org/addon/ublock-origin/), [*Chrome*](https://chrome.google.com/webstore/detail/ublock-origin/cjpalhdlnbpafiamejdnhcphjbkeiagm)).

<a name="gqy8gv5w1rzc"></a>

#### e. Digital security concerns

- **Suspicious email or suspected phishing**

    If you suspect an email you have received is a phishing attack, or it makes you suspicious that it is not genuine in any way, don't click on any link or download any attachment and please alert the {ORG Director of Operations}.

    After alerting the {ORG Director of Operations}, please forward the full original email to Access Now’s Helpline Team at
[*help@accessnow.org*](mailto:help@accessnow.org) with the reasons you
are concerned.

    To forward the full original email, please follow [these instructions](https://circl.lu/pub/tr-07/).

    Once again, be extremely careful not to click on the links nor
download attachments from the suspicious message, and don’t forget to
alert anyone you are sending the message to about the danger.

-   **Digital security emergencies**

    If a physical emergency happens, please alert the {ORG Executive
Director} and refer to {the Staff Safety and Security Protocols} for
the approved channel of contact.

    If there is any potential digital security concerns, including
clicking a potential phishing or infected link or attachment, please
escalate to the {Executive Director} and {Operations Director} as soon
as possible.

    It is very important to quickly alert someone to any digital security threat you perceive. It can be hard to admit when one has made a mistake, but if you believe you did something to compromise the organization or a partner or member, it is better to let someone know so the damage can be mitigated. Telling someone about the mistake promptly is the best thing to do.

<a name="cwhasyrukd3s"></a>

### 4. Travel policies and procedures

When traveling, it is especially important to be aware of one’s
surroundings, and the local laws governing the destination. At any
point, especially when crossing borders, your devices may be searched or
taken out of view.

More details on travel security can be found in [this guide](https://guides.accessnow.org/safer_travel_guide.html)

<a name="66ctgfndt3o"></a>

#### a. Devices

[**Full disk encryption**](#g47kclkb06ga) **becomes essential** on all
phones, laptops, and tablets. It works when you turn off your device
before crossing a border, or entering any travel security checkpoint.

**Leave devices that you don't need at home**. Try to only bring devices
that you are sure are essential to your work. Avoid any devices that
hold particularly sensitive or confidential information.

**Transport data only on encrypted media**. If you are using external
media, like USBs, SD cards, and hard drives, encrypt the device before
storing anything on it.

Double check all of your devices **have passwords, PINS, and codes** for
access.

<a name="wf4menzgiaqi"></a>

#### b. Internet use

When browsing the internet away from home, **always use a VPN** to
connect. Be sure to install the VPN on all phones, laptops, and tablets
you’ll be bringing with you.

Clear your cache, cookies, and browsing history across all browsers
(mobile phone included):

- [*Firefox*](https://support.mozilla.org/en-US/kb/how-clear-firefox-cache)

- [*Chrome*](https://support.google.com/chrome/answer/95582?hl=enhttps://support.google.com/chrome/answer/95582?hl=en)

- [*Other Browsers*](https://kb.iu.edu/d/ahic)

Clear passwords from all browsers:

- [*Chrome*](https://support.google.com/chrome/answer/95606?hl=en)

- [*Firefox*](https://support.mozilla.org/en-US/kb/password-manager-remember-delete-change-and-import)

<a name="6bqo8dqlb1im"></a>

### 5. Creating and maintaining an engaged organizational culture

To continue to keep the organization safe and secure, please keep good
"security hygiene" in mind. Update all devices promptly and routinely,
and remain aware of your surroundings and of where your devices are
(hopefully on your person!). Consider installing security enhancing
browser extensions, like ad-blockers, and chat with coworkers about
their security practices. Speak with partners to find out what their
concerns are, and create plans to deal with the immediate threats and
risks they face.

Sharing relevant news articles from reputable media, [*interactive games
and quizzes*](https://orgsec.community/display/OS/Ways+to+build+awareness+around+security+issues), and [*community
stories*](https://orgsec.community/display/OS/Why+should+the+organisation+and+its+staff+care+about+security) all help engage different staff. Have open communications channels with
staff to ensure that any questions or issues are raised early.

Remember how everyone in the organization is connected. Your practices
at home affect your security at work. Keep your personal email and
social media accounts as secure as your work-related ones. You can apply
all of the practices for your work devices discussed here to your
personal devices.


