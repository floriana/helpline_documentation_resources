<div align="center"><img src="images/HLLogo.png"></div>

# <div align="center">[ORG NAME]</div>

# <div align="center">Rapport d'évaluation de sécurité</div>

<div align="center">Version [Version Number] - [Date]</div>

--------------------------------------------------------------------------
<div style="background-color:#ccff99">
Le document confidentiel suivant décrit l’évaluation initiale des pratiques de sécurité numérique à **[ORG NAME]** selon la Digital Security Helpline d' Access Now. Dans ce document, nous présentons une brève revue des pratiques mises en place actuellement, suivie par une proposition d'une liste de recommendation d'améliorations, classées en ordre de priorité.
</div>
--------------------------------------------------------------------------

## Le Contexte

[this is the Background context of organization] - see [instructions in the "Background" section](README.md#background).


##  Les Défis Identifiés

Durant une évaluation initiale de la sécurité de l'association documenté au ticket [accessnow **[Case ID]**] nous avons identifié les améliorations suivantes: 

<div style="color:red">[what follows is a list of examples, please use the ones that apply or add new ones to your report]</div>

- <div style="color:#A9A9A9">Chiffrement complet des machines.</div>
- <div style="color:#A9A9A9">Création et chiffrement des dossiers de sauvegarde pour le données sensibles.</div>
- <div style="color:#A9A9A9">Création d'une politique de sauvegarde des données</div>
- <div style="color:#A9A9A9">Recrutement d'un webmaster à plein temps </div>
- <div style="color:#A9A9A9">Protection des données matérielles avec un coffre-fort</div>
- <div style="color:#A9A9A9">Évaluation de la sécurité de Système de référencement</div>
- <div style="color:#A9A9A9">Mise en place de gestionnaires de mot de passe </div>
- <div style="color:#A9A9A9">Activation de la vérification à deux étapes dans les comptes emails et les comptes des réseaux sociaux.</div>
- <div style="color:#A9A9A9">Renforcement des capacités des équipes en hygiéne informatique de base, y compris la communication sécurisée</div>
- <div style="color:#A9A9A9">Amélioration de la sécurité du site web</div>


****
<div style="color:red">[What follows is a list of examples. Please use the ones that apply, edit the descriptions, and/or add new items to your report. Items could have different priorities than in the examples, and based on the results of the assessment may be assigned a higher or lower priority.]</div>

## Le Plan de Restauration

Afin de relever les défis présentés ci-dessus, nous sommes disponible pour aider **[ORG NAME]** dans la conception, la priorisation et la mise en œuvre d'un plan d'aide.

Si le plan est accepté, notre équipe créera des threads dédiés avec le personnel de **[ORG NAME]** pour chaque sujet de correction et inclura les informations associées permettant de soutenir la mise en œuvre.

Nous recommandons que le personnel de **[ORG NAME]** dirige la mise en œuvre du plan de correction et obtient le soutien et l'appui nécessaires du reste de l'organisation pour mettre en œuvre ces nouvelles pratiques organisationnelles.


**<div align="center">Tâches prioritaires</div>**


#### <div style="color:#A9A9A9">Chiffrement complet des disques durs</div>
<div style="color:#A9A9A9">Les ordinateurs de l 'association utilisent le systéme d'exploitation Windows selon notre interlocuteur. Nous devons donc initialement examiner si Bitlocker (le programme de chiffrement par défaut) est disponible. Ensuite nous devons l'activer dans toutes les machines. Si la version ou la licence ne permet pas, nous allons aider à recevoir le mise à jour nécessaire pour activer Bitlocker.</div>


#### <div style="color:#A9A9A9">Cartographie des données(Data Mapping)</div>
<div style="color:#A9A9A9">
Cette tache est nécessaire pour les taches qui suivent, car elle nous permettra l'identification, la localisation et la classification des données. Ensuite nous devons identifier les données les plus sensibles qui doivent être sauvegarder et crypter individuellement.
</div>

#### <div style="color:#A9A9A9">Cryptage des données individuellement</div>
<div style="color:#A9A9A9">En plus du chiffrement complet des disques durs et suite à la cartographie des données, il est important de chiffrer les données les plus importantes et les plus sensibles individuellement. Nous pouvons utiliser un programme comme [VeraCrypt](https://www.veracrypt.fr).
</div>


#### <div style="color:#A9A9A9">Politique de sauvegarde (backup)</div>

<div style="color:#A9A9A9">
En ce basant sur le type des systèmes d'opération utilisé (Windows) par l'organisation, nous devons concevoir une politique de sauvegarde des données pour sauvegarder les données importants et déterminer comment les restaurer suite à un incident.
</div>


#### <div style="color:#A9A9A9">Équiper les offices par des coffre-forts</div>

<div style="color:#A9A9A9">L'exercice de cartographie des données va absolument identifier des données en papier. Ce type de données qui peut contenir des informations sensibles, doit être sauvegardé dans un coffre-fort.</div> 


**<div align="center">Tâches de priorité moyenne</div>**

#### <div style="color:#A9A9A9">Évaluation de la sécurité de Système de référencement</div>

<div style="color:#A9A9A9">L'organisation utilise un système de référencement pour recevoir et documenter des cas sensibles. Il est important donc d'évaluer de las sécurité de ce système et s'assurer qu'il reçoit des mesures nécessaires pour protéger les données des victimes et leur vie privée.
</div>

#### <div style="color:#A9A9A9">Destruction des données non nécessaires</div>

<div style="color:#A9A9A9">Détruire les données(papier ou numérique) lorsqu'ils ne sont plus nécessaires.</div>

#### <div style="color:#A9A9A9">Adaptation de communications cryptées</div>

<div style="color:#A9A9A9">Nous trouvons que c'est important pour la sécurité de l'organisation ainsi que la sécurité de ses clients d'adopter des outils chiffrés pour la communication. Donc nous proposons d'utiliser des outils tel que [OepnPGP](https://www.openpgp.org/) pour les emails et [Signal](https://www.signal.org/fr/) ou [Wire](https://wire.com/) pour les communications instantanées ou chat.</div>


#### <div style="color:#A9A9A9">Authentification à deux étapes</div>
<div style="color:#A9A9A9">Dans quelques scénarios possible, les boites de courrier ainsi que d'autres comptes en ligne qui appartiennent à l'organisation, peuvent être ciblés par des attaques de type hameçenage(phishing) dans le but de voler leurs mots de passe. Il est important donc d'activer l'authentification à deux étapes dans tout les comptes qui supportent cette option.</div>

#### <div style="color:#A9A9A9">Mots de passe Uniques et Forts + Gestionnaire des mots de passe(Password Manager)</div>
<div style="color:#A9A9A9">Dans le même cadre, l'organisation doit adopter une politique qui force l'utilisation des mots de passe unique et forts pour tout le staff et dans pour tous les comptes qui appartient à l'organisation. Un gestionnaire de mot de passe va aider à adopter cette politique. </div>

#### <div style="color:#A9A9A9">VPN pour Antennes surtout!</div>
<div style="color:#A9A9A9">
Nous avons identifié la présence des membres qui travaillent individuellement depuis d'autre villes(Antennes). Ils est important de leur allouer des comptes VPN fiables pour protéger leurs connections, lorsqu'ils consultent ou contribuent à des projets qui appartiennent à l'organisation.</div>


**<div align="center">Autres tâches moins prioritaires</div>**

#### <div style="color:#A9A9A9">Formation en meilleures pratiques de sécurité</div>
<div style="color:#A9A9A9">
Suite à l'introduction de tout ces nouvelles solution discutées si-dessus, il est important de fournir des formations à l'équipe. Ces formations auront pour sujet : l'hygiéne numérique de base. La formation couvrira les solutions proposées, mais aussi, d'autres bonne pratiques quotidiennes.</div>

#### <div style="color:#A9A9A9">Recrutement d'un webmaster et un IT à plein temps</div>
<div style="color:#A9A9A9">La maintenance de site web de l'organisation est assurée par un externe. Cela peut ajouter un risque sur la sécurité de site et l'organisation en général. Il est important donc de recruter un staff pour compléter cette tache. Il y a aussi un besoin de recruter un professionnel spécialisé en informatique. Il aura pour tache de maintient les machines et les solutions informatiques utilisées par l'organisation.</div>

#### <div style="color:#A9A9A9">Créer un plan de fuite de données</div>
<div style="color:#A9A9A9">Créez un plan pour traiter une éventuelle fuite des données afin de contenir le problème, de sécuriser les systèmes et les canaux de communication de l'organisation et d'atténuer les dommages pouvant avoir été causés par la fuite.</div>

#### <div style="color:#A9A9A9">SSL et Protection contre DDoS</div>
<div style="color:#A9A9A9">Assurer la protection des visiteurs de site web par ajouter un certificat SSL. Il est recommandé aussi d'ajouter une protection contre DDoS attaques qui menacent la présence de site à tout moment.</div>

#### <div style="color:#A9A9A9">Création d'une politique de sécurité</div>
<div style="color:#A9A9A9">A fin d'assurer ces recommandations de sécurité, nous recommandons aussi la création d'un document qui trace la politique de sécurité pour l'organisation. Ce document doit être une référence et toujours accessible par les membres de l'équipe de l'organisation.</div>

--------------------------------------------------------------------------
**<div style="background-color:#ccff99">[Author Name]</div>**

<div style="background-color:#ccff99">
[author title] | [author name]@accessnow.org</div>
--------------------------------------------------------------------------
<div style="background-color:#ccff99">
Pour plus d'information, visitez: <a href="https://www.accessnow.org/help">accessnow.org/help</a></div>

<div style="background-color:#ccff99">
Access Now Helpline Data Usage Policy and Terms of Service: <a href="https://www.accessnow.org/data-usage-policy/">www.accessnow.org/data-usage-policy</a></div>
                 
--------------------------------------------------------------------------
